#!/bin/bash

# SPDX-License-Identifier: BSD-3-Clause

# toolchain
source /morello/env/morello-sdk

# kernel
wget https://git.morello-project.org/morello/kernel/linux/-/archive/morello/master/linux-morello-master.tar.gz
tar xf linux-morello-master.tar.gz
cd linux-morello-master

# install headers
make headers_install HOSTCC=clang CC=clang ARCH=arm64 INSTALL_HDR_PATH=${CI_PROJECT_DIR}/usr
