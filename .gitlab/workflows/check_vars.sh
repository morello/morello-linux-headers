#!/bin/bash

# SPDX-License-Identifier: BSD-3-Clause

if [[ -z "$RELEASE_NAME" ]]; then
    echo "Must provide RELEASE_NAME in environment for commit name: \"morello: Update headers for \$RELEASE_NAME\"" 1>&2
    exit 1
fi
