# Morello Linux Kernel Headers

The headers contained in this repository were generated from the Morello Kernel [1] using the command:
```
$ make headers_install HOSTCC=clang CC=clang ARCH=arm64 INSTALL_HDR_PATH=<install dir>
```
Previous version of the headers were generated from [2].

[1] https://git.morello-project.org/morello/kernel/linux  
[2] https://git.morello-project.org/morello/kernel/morello-ack
