/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
#ifndef __ASM_CHERI_H
#define __ASM_CHERI_H

/*
 * VMem software-defined capability permission, assigned to the User[0]
 * permission on Morello (bit 2).
 */
#define CHERI_PERM_SW_VMEM	(1 << 2)

#endif /* __ASM_CHERI_H */
